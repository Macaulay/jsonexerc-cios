﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RandomUserJson.Models
{
    class RandomUser
    {
        public List<Result> Results { get; set; }
        
        //
        //public string Street { get; set; }
        //public string City { get; set; }
        //public string State { get; set; }
        //public string PostCode { get; set; }
        //public string Coordinates { get; set; }
        //public string Latitude { get; set; }
        //public string Longitude { get; set; }
        //public string TimeZone { get; set; }
        //public string OffSet { get; set; }
        //public string Description { get; set; }
        //public string Email { get; set; }

    }

    class Result
    {
        public string Gender { get; set; }
        public Name Name { get; set; }
        public Location Location { get; set; }
    }
    class Name
    {
        public string Title { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
    }
    class Location
    {
        public Street Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public Coordinates Coordinates { get; set; }
        public TimeZone TimeZone { get; set; }
    }
    class Street
    {
        public string Number { get; set; }
        public string Name { get; set; }
    }
    class Coordinates
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
    class TimeZone
    {
        public string OffSet { get; set; }
        public string Description { get; set; }
    }


    //public class Coordinates
    //{

    //}
    //public class TimeZone
    //{

    //}
}
