﻿using Newtonsoft.Json;
using System;
using System.Net;
using RandomUserJson.Models;

namespace RandomUserJson
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n***Gostaria de fazer a busca de um Usuário?***\n+: Para sim\nOu aperte a tecla 'Enter' para encerrar.");

            string opc;
            opc = Console.ReadLine();
            if (opc == "+")
            {
                Console.WriteLine("Digite o número da quantidade de usuários desejados:\n ");
                string num;
                num = Console.ReadLine();
                Console.WriteLine("Digite a nacionalidade do(s) usuário(os) desejado(os): *EM UF*\n ");
                string nac;
                nac = Console.ReadLine();
                string url = "https://randomuser.me/api/?results="+ num + "&nat=" + nac;

                RandomUser RandomUser = BuscarRandomUser(url);


                Console.WriteLine("Dados de Usuario:\n");

                foreach (Result Result in RandomUser.Results)
                {
                    Console.WriteLine("Result:\n");

                    Console.WriteLine("Gender: " + Result.Gender);
                    Console.WriteLine("Title: " + Result.Name.Title);
                    Console.WriteLine("First: " + Result.Name.First);
                    Console.WriteLine("Last: " + Result.Name.Last);

                    Console.WriteLine("\nLocation:\n");

                    Console.WriteLine("Number: " + Result.Location.Street.Number);
                    Console.WriteLine("Name: " + Result.Location.Street.Name);

                    Console.WriteLine("City: " + Result.Location.City);
                    Console.WriteLine("State: " + Result.Location.State);
                    Console.WriteLine("Country: " + Result.Location.Country);
                    Console.WriteLine("PostCode: " + Result.Location.PostCode);

                    Console.WriteLine("\nCoordinates:\n");

                    Console.WriteLine("Latitude: " + Result.Location.Coordinates.Latitude);
                    Console.WriteLine("Longitude: " + Result.Location.Coordinates.Longitude);

                    Console.WriteLine("\nTimeZone:\n");

                    Console.WriteLine("OffSet: " + Result.Location.TimeZone.OffSet);
                    Console.WriteLine("Description: " + Result.Location.TimeZone.Description);
                }

            }

            

            
            
           

            Console.WriteLine("\n\n");

            Console.ReadLine();
        }
        public static RandomUser BuscarRandomUser(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);
            
            //Console.WriteLine(content);
            Console.WriteLine("\n\n");

            RandomUser RandomUser = JsonConvert.DeserializeObject<RandomUser>(content);
       
            return RandomUser;
        }
    }
}

