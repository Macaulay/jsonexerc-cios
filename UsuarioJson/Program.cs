﻿using Newtonsoft.Json;
using System;
using System.Net;
using UsuarioJson.Models;

namespace UsuarioJson
{
    class Program
    {
        static void Main(string[] args)
        {
            // url base da API
            string url = "http://senacao.tk/objetos/usuario";

            // consumindo API
            Usuario Usuario = BuscarUsuario(url);

            // exibindo dados retornados (Podendo utilizar o modo WriteLine para cada informação)
            Console.WriteLine(
                string.Format(
                    "Nome: {0} - Email {1} - Telefone {2}",
                    Usuario.Nome,
                    Usuario.Email,
                    Usuario.Telefone
                    )
                );
            Console.WriteLine("\nConhecimentos:\n");

            foreach(string op in Usuario.Conhecimentos)
            {
                Console.WriteLine("- " + op);
            }
        
            Console.WriteLine("\nDados do endereço:\n");
            Console.WriteLine("Rua: " + Usuario.Endereco.Rua);
            Console.WriteLine("Número: " + Usuario.Endereco.Numero);
            Console.WriteLine("Bairro: " + Usuario.Endereco.Bairro);
            Console.WriteLine("Cidade: " + Usuario.Endereco.Cidade);
            Console.WriteLine("UF: " + Usuario.Endereco.UF);

            Console.WriteLine("\nQualificações:\n");

            foreach (Qualificacao Qualificacao in Usuario.Qualificacoes)
            {
                Console.WriteLine("- " + Qualificacao.Nome);
                Console.WriteLine("- " + Qualificacao.Instituicao);
                Console.WriteLine("- " + Qualificacao.Ano + "\n");
            }

            // mantendo o console aberto
            Console.ReadLine();
        }
        public static Usuario BuscarUsuario(string url)
        {
            // Instanciando WebClient para chamadas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            Usuario usuario = JsonConvert.DeserializeObject<Usuario>(content);

            // retornando computador
            return usuario;
        }
    }
}

