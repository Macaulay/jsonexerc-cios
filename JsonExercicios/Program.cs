﻿using Newtonsoft.Json;
using System;
using System.Net;
using VeiculosJson.Models;

namespace VeiculosJson
{
    class Program
    {        
        static void Main(string[] args)
        {
            string url = "http://senacao.tk/objetos/veiculo";

            Veiculo Veiculo = BuscarVeiculo(url);

            Console.WriteLine(
                string.Format(
                    "Marca: {0} - Modelo {1} - Ano {2}, Quilometragem {3}",
                    Veiculo.Marca,
                    Veiculo.Modelo,
                    Veiculo.Ano,
                    Veiculo.Quilometragem)                  
                    );
           
            Console.ReadLine();
        }
        public static Veiculo BuscarVeiculo(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            Veiculo Veiculo = JsonConvert.DeserializeObject<Veiculo>(content);
            return Veiculo;
        }
    }
}
