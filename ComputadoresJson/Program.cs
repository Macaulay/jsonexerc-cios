﻿using Newtonsoft.Json;
using System;
using System.Net;
using ComputadoresJson.Models;

namespace ComputadoresJson
{
    class Program
    {
        static void Main(string[] args)
        {
            // url base da API
            string url = "http://senacao.tk/objetos/computador";

            // consumindo API
            Computador Computador = BuscarComputador(url);

            // exibindo dados retornados
            Console.WriteLine(
                string.Format(
                    "Marca: {0} - Modelo {1} - Memoria {2}, SSD {3}",
                    Computador.Marca,
                    Computador.Modelo,
                    Computador.Memoria,
                    Computador.SSD
                    )
                );

            // mantendo o console aberto
            Console.ReadLine();
        }

        // função destinada consumir a API que retornará os dados de um computador 
        public static Computador BuscarComputador(string url)
        {
            // Instanciando WebClient para chamadas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            Computador Computador = JsonConvert.DeserializeObject<Computador>(content);

            // retornando computador
            return Computador;
        }
    }
}
