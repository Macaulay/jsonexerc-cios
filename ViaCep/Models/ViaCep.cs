﻿using System;
using System.Collections.Generic;
using System.Text;


namespace ViaCepJson.Models
{
    class ViaCep
    {
        public string CEP { get; set; }
        public string Logradouro { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Localidade { get; set; }
        public string UF { get; set; }
        public string Unidade { get; set; }
        public int IBGE { get; set; }
        public int GIA { get; set; }
    }
}
