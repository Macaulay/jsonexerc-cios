﻿using Newtonsoft.Json;
using System;
using System.Net;
using ViaCepJson.Models;

namespace ViaCepJson
{
   
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n***Gostaria de fazer a busca de um CEP?***\n+: Para sim\nOu aperte a tecla 'Enter' para encerrar.");

            string opc;
            opc = Console.ReadLine();
            if (opc == "+")
            {
                Console.WriteLine("Digite o CEP desejado: ");
                string cep;
                cep = Console.ReadLine();
                string url = "https://viacep.com.br/ws/" + cep + "/json/";

                ViaCep ViaCep = BuscarCep(url);

                Console.WriteLine("\nDados de Endereço:\n");
                Console.WriteLine("CEP: " + ViaCep.CEP);
                Console.WriteLine("Logradouro: " + ViaCep.Logradouro);
                Console.WriteLine("Complemento: " + ViaCep.Complemento);
                Console.WriteLine("Bairro: " + ViaCep.Bairro);
                Console.WriteLine("Unidade: " + ViaCep.Unidade);
                Console.WriteLine("IBGE: " + ViaCep.IBGE);
                Console.WriteLine("GIA: " + ViaCep.GIA);
                Console.ReadLine();
            }

        }
        public static ViaCep BuscarCep(string url)
        {
            // Instanciando WebClient para chamadas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            //Console.WriteLine(content);
            //Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            ViaCep ViaCep = JsonConvert.DeserializeObject<ViaCep>(content);

            // retornando computador
            return ViaCep;
        }
    }
}
